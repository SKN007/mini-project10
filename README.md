# Mini projrct 10

Rust Serverless Transformer Endpoint

## Requiements

Dockerize Hugging Face Rust transformer

Deploy container to AWS Lambda

Implement query endpoint

## Steps

1. Create a lambda function.

   ```
   cargo lambda new mini-project10
   ```
2. Write code in main.rs and download the .bin model used in the code.
3. Create and write a Dockerfile.
4. In AWS, create a new repository in ECR.
5. Build and push to ECR.
6. In AWS, create a function using the image in the ECR just created.
7. Create a cURL and then we can test with the cURL.

### Result display

[My app.](https://mini-project9-uwjefnjzdm22nxekgfe2zu.streamlit.app)

![img](sc1.png)
